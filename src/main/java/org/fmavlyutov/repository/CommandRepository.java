package org.fmavlyutov.repository;

import org.fmavlyutov.api.ICommandRepository;
import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandLineConstant.HELP, CommandLineArgument.HELP, "display info about commands and arguments");

    public static final Command VERSION = new Command(CommandLineConstant.VERSION, CommandLineArgument.VERSION, "display program version");

    public static final Command ABOUT = new Command(CommandLineConstant.ABOUT, CommandLineArgument.ABOUT, "display info about author");

    public static final Command INFO = new Command(CommandLineConstant.INFO, CommandLineArgument.INFO, "display system info");

    public static final Command COMMANDS = new Command(CommandLineConstant.COMMANDS, CommandLineArgument.COMMANDS, "display all commands");

    public static final Command ARGUMENTS = new Command(CommandLineConstant.ARGUMENTS, CommandLineArgument.ARGUMENTS, "display all arguments");

    public static final Command EXIT = new Command(CommandLineConstant.EXIT, null, "finish program");

    public static final Command PROJECT_CREATE = new Command(CommandLineConstant.PROJECT_CREATE, null,"create new project");

    public static final Command PROJECT_LIST = new Command(CommandLineConstant.PROJECT_LIST, null, "show list of projects");

    public static final Command PROJECT_CLEAR = new Command(CommandLineConstant.PROJECT_CLEAR, null, "delete all projects");

    public static final Command TASK_CREATE = new Command(CommandLineConstant.TASK_CREATE, null,"create new task");

    public static final Command TASK_LIST = new Command(CommandLineConstant.TASK_LIST, null, "show list of tasks");

    public static final Command TASK_CLEAR = new Command(CommandLineConstant.TASK_CLEAR, null, "delete all tasks");

    public static final Command[] TERMINAL_COMMANDS = new Command[] {HELP, VERSION, ABOUT, INFO, EXIT, COMMANDS, ARGUMENTS, PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR, TASK_CREATE, TASK_LIST, TASK_CLEAR};

    public Command[] getCommands() {
        return TERMINAL_COMMANDS;
    }

}
