package org.fmavlyutov.repository;

import org.fmavlyutov.api.IProjectRepository;
import org.fmavlyutov.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

}
