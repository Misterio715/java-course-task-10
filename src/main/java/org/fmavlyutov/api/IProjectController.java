package org.fmavlyutov.api;

public interface IProjectController {


    void createProject();

    void clearProjects();

    void showProjects();

}
