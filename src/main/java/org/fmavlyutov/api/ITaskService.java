package org.fmavlyutov.api;

import org.fmavlyutov.model.Task;

public interface ITaskService extends ITaskRepository{

    Task create(String name, String description);

}
