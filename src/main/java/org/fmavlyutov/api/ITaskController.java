package org.fmavlyutov.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
