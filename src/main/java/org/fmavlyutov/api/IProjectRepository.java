package org.fmavlyutov.api;

import org.fmavlyutov.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void clear();

    List<Project> findAll();

}
