package org.fmavlyutov.api;

import org.fmavlyutov.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);
}
