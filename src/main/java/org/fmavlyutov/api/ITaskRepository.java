package org.fmavlyutov.api;

import org.fmavlyutov.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void clear();

    List<Task> findAll();

}
