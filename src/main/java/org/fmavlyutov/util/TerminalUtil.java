package org.fmavlyutov.util;

import java.util.Scanner;

import static java.lang.System.in;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(in);

    static String nextline() {
        return SCANNER.nextLine();
    }

}
