package org.fmavlyutov.service;

import org.fmavlyutov.api.IProjectRepository;
import org.fmavlyutov.api.IProjectService;
import org.fmavlyutov.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void add(Project project) {
        if (project == null) {
            return;
        }
        projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (description == null) {
            return null;
        }
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }
}
