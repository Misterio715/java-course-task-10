package org.fmavlyutov.service;

import org.fmavlyutov.api.ITaskRepository;
import org.fmavlyutov.api.ITaskService;
import org.fmavlyutov.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void add(Task task) {
        if (task == null) {
            return;
        }
        taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (description == null) {
            return null;
        }
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }
}
