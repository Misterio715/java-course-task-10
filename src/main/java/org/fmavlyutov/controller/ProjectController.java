package org.fmavlyutov.controller;

import org.fmavlyutov.api.IProjectController;
import org.fmavlyutov.api.IProjectService;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        String name = TerminalUtil.nextline();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextline();
        Project project = projectService.create(name, description);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
        } else {
            System.out.println("[SUCCESSFUL]\n");
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECTS LIST]");
        List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            if (project == null) {
                continue;
            }
            System.out.println(index++ + ". " + project.getName());
        }
        System.out.println("[SUCCESSFUL]\n");
    }
}
