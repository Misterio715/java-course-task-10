package org.fmavlyutov.controller;

import org.fmavlyutov.api.ITaskController;
import org.fmavlyutov.api.ITaskService;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        String name = TerminalUtil.nextline();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextline();
        Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
        } else {
            System.out.println("[SUCCESSFUL]\n");
        }
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASKS LIST]");
        List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            if (task == null) {
                continue;
            }
            System.out.println(index++ + ". " + task.getName());
        }
        System.out.println("[SUCCESSFUL]\n");
    }
}
