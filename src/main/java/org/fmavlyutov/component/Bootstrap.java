package org.fmavlyutov.component;

import org.fmavlyutov.api.*;
import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.controller.CommandController;
import org.fmavlyutov.controller.ProjectController;
import org.fmavlyutov.controller.TaskController;
import org.fmavlyutov.repository.CommandRepository;
import org.fmavlyutov.repository.ProjectRepository;
import org.fmavlyutov.repository.TaskRepository;
import org.fmavlyutov.service.CommandService;
import org.fmavlyutov.service.ProjectService;
import org.fmavlyutov.service.TaskService;
import org.fmavlyutov.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void start(String[] args) {
        argumentsProcessing(args);
        commandsProcessing();
    }

    private void argumentsProcessing(String[] args) {
        commandController.displayHello();
        if (args == null || args.length == 0) {
            return;
        }
        for (String arg : args) {
            switch (arg) {
                case CommandLineArgument.HELP:
                    commandController.displayHelp();
                    break;
                case CommandLineArgument.VERSION:
                    commandController.displayVersion();
                    break;
                case CommandLineArgument.ABOUT:
                    commandController.displayAbout();
                    break;
                case CommandLineArgument.INFO:
                    commandController.displayInfo();
                    break;
                case CommandLineArgument.COMMANDS:
                    commandController.displayCommands();
                    break;
                case CommandLineArgument.ARGUMENTS:
                    commandController.displayArguments();
                    break;
                default:
                    commandController.displayArgumentError();
            }
        }
    }

    private void commandsProcessing() {
        String arg = "";
        while (!arg.equals(CommandLineConstant.EXIT)) {
            arg = TerminalUtil.nextline();
            displayCommand(arg);
        }
    }

    private void displayCommand(String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case CommandLineConstant.HELP:
                commandController.displayHelp();
                break;
            case CommandLineConstant.VERSION:
                commandController.displayVersion();
                break;
            case CommandLineConstant.ABOUT:
                commandController.displayAbout();
                break;
            case CommandLineConstant.INFO:
                commandController.displayInfo();
                break;
            case CommandLineConstant.COMMANDS:
                commandController.displayCommands();
                break;
            case CommandLineConstant.ARGUMENTS:
                commandController.displayArguments();
                break;
            case CommandLineConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandLineConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandLineConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandLineConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandLineConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandLineConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandLineConstant.EXIT:
                exit();
                break;
            default:
                commandController.displayCommandError();
        }
    }

    private void exit() {
        commandController.displayGoodbye();
        System.exit(0);
    }

}
